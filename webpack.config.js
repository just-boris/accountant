/*eslint-env node*/
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

function configFactory(hotload) {
    var config = {
        entry: ['./src/index.js'],
        output: {
            path: path.join(__dirname, 'dist'),
            pathinfo: hotload,
            filename: '[name].js'
        },
        module: {
            loaders: [{
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }, {
              test: /\.(png|svg|woff2?|ttf|eot)(\?.*)?$/,
              loader: 'url-loader'
          }, {
                test:   /\.css$/,
                loader: hotload ? 'style-loader!css-loader!' : ExtractTextPlugin.extract('style-loader', 'css-loader')
            }, {
                test: /\.hbs/,
                loader: 'handlebars-loader',
                query: {
                    helperDirs: [
                        path.join(__dirname, 'src/helpers')
                    ]
                }
            }]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html',
                inject: 'body'
            }),
            new ExtractTextPlugin('styles.css')
        ]
    };
    if(hotload) {
        config.plugins.push(new webpack.HotModuleReplacementPlugin());
        config.devtool = 'source-map';
    } else {
        config.plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true}));
    }
    return config;
}

module.exports = configFactory(false);
module.exports.configFactory = configFactory;
