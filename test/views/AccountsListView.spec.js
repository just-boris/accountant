import AccountsListView from '../../src/views/accounts-list/AccountsListView';
import AppModel from '../../src/models/AppModel';

describe('AccountsListView', function() {
  beforeEach(function() {
    this.model = new AppModel();
    this.view = new AccountsListView({model: this.model}).render();
    this.el = this.view.$el;
  });

  it('should show empty test if there are no accounts', function() {
    expect(this.el.find('.accounts__empty')).toHaveCss({display: 'block'});
  });

  it('should add and remove accounts', function() {
    this.el.find('.accounts__add').click();
    expect(this.el.find('.account')).toHaveLength(1);
    expect(this.el.find('.accounts__empty')).toHaveCss({display: 'none'});

    this.el.find('.account__remove').click();
    expect(this.el.find('.account')).toHaveLength(0);
    expect(this.el.find('.accounts__empty')).toHaveCss({display: 'block'});
  });
});
