import AppView from '../../src/views/app/AppView.js';
import router from '../../src/router';

describe('AppView', function() {
  beforeEach(function() {
    spyOn(router, 'toUrl');
    this.view = new AppView().render();
    this.el = this.view.$el;
  });

  it('should render personal step by default', function() {
    expect(this.el.find('.modal-title')).toHaveText('Personal information');
  });

  it('should update view on route change', function() {
    router.trigger('route', 'step', ['accounts']);
    expect(this.el.find('.modal-title')).toHaveText('Bank accounts');
  });

  it('should treat unknown steps as "personal"', function() {
    router.trigger('route', 'step', ['random step']);
    expect(this.el.find('.modal-title')).toHaveText('Personal information');
  });

  it('should switch to next step', function() {
    this.view.nextStep();
    expect(router.toUrl).toHaveBeenCalledWith('accounts');
    this.view.nextStep();
    expect(router.toUrl).toHaveBeenCalledWith('success');
  });

  it('should not switch to step out of range', function() {
    this.view.currentStep = 2;
    this.view.nextStep();
    expect(router.toUrl).not.toHaveBeenCalled();
  });

  it('should not allow negative step index', function() {
    this.view.previousStep();
    expect(router.toUrl).not.toHaveBeenCalled();
  });

  it('should switch back to prevous step', function() {
    this.view.currentStep = 1;
    this.view.previousStep();
    expect(router.toUrl).toHaveBeenCalledWith('personal');
  });
});
