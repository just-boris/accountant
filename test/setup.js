/* eslint-env node */
//jsdom
const jsdom = require('jsdom').jsdom;
global.document = jsdom('<html><head></head><body></body></html>', {
    url: 'http://localhost'
});
global.window = global.document.defaultView;
global.window.localStorage = jasmine.createSpyObj('localStorage', ['getItem', 'setItem']);
global.navigator = global.window.navigator;
global.location = global.window.location;

const jQuery = require('jquery');
global.window.jQuery = jQuery;

//mock jquery plugins
jQuery.fn.datepicker = function() {};
jQuery.fn.modal = function() {};

//jasmine addons
global.joc = jasmine.objectContaining;
global.jany = jasmine.any;
require('jasmine-jquery');

//require hooks
const Handlebars = require('handlebars');
require.cache[require.resolve('handlebars/runtime')] = {exports: Handlebars};
require.extensions['.css'] = function() {};
require('babel-register')({sourceMaps: 'inline'});
