import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker3.css';
import $ from './jquery';
import 'bootstrap/js/modal';
import 'bootstrap-datepicker';
import Backbone from 'backbone';
import Syphon from 'backbone.syphon';
import AppView from './views/app/AppView';

Syphon.InputReaders.register('date', ($el) => $el.val());

$(document).ready(() => {
    window.app = new AppView({el: '#app'}).render();
    Backbone.history.start();
});
