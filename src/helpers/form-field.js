/*eslint-env node*/
import template from './form-field.hbs';
import Handlebars from 'handlebars/runtime';

//handlebars-loader can't work with es6 exports,
//https://github.com/altano/handlebars-loader/pull/65
module.exports = function({hash}) {
  const result = template(Object.assign({
    type: 'text'
  }, hash));
  return new Handlebars.SafeString(result);
};
