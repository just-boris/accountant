import Backbone from 'backbone';

const DATA_KEY = 'accountData';

export default class AppModel extends Backbone.Model {
  defaults() {
    return {
      accounts: []
    };
  }

  fetch() {
    return new Promise(resolve => {
      const settings = window.localStorage.getItem(DATA_KEY);
      this.set(JSON.parse(settings));
      resolve();
    });
  }

  save(key, val) {
    this.set(key, val);
    const json = this.toJSON();
    window.localStorage.setItem(DATA_KEY, JSON.stringify(json));
  }
}
