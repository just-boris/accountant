import BaseStepView from '../base-step/BaseStepView';
import Syphon from 'backbone.syphon';
import template from './PersonalView.hbs';

export default class PersonalView extends BaseStepView {
  get tagName() {
    return 'form';
  }

  get className() {
    return 'personal';
  }

  events() {
    return {
      'submit': 'saveData'
    };
  }

  initialize() {
    this.template = template;
  }

  render() {
    super.render();
    this.$('.input-group.date').datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true
    });
    return this;
  }

  saveData(e) {
    e.preventDefault();
    this.model.save(Syphon.serialize(this));
    this.trigger('submit');
  }
}
