import BaseStepView from '../base-step/BaseStepView';
import _ from 'underscore';
import AccountsCollection from '../../collections/AccountsCollection';
import template from './AccountsListView.hbs';
import AccountView from '../account/AccountView';

export default class AccountsListView extends BaseStepView {
  get tagName() {
    return 'form';
  }

  get className() {
    return 'accounts';
  }

  events() {
    return {
      'submit': 'saveData',
      'click .accounts__previous': 'gotoPreviousStep',
      'click .accounts__add': 'addAccount'
    };
  }

  initialize() {
    this.template = template;
    this.collection = new AccountsCollection(this.model.get('accounts'));
    this.accountViews = [];
  }

  render() {
    super.render();
    this.emptyText = this.$('.accounts__empty');
    this.accountsCountValidationText = this.$('.accounts__min-text');
    this.accountsContainer = this.$('.accounts__items');
    this.collection.forEach(this.renderAccount, this);
    this.emptyText.toggle(this.collection.length === 0);
    return this;
  }

  renderAccount(account) {
    const view = new AccountView({model: account}).render();
    view.on('remove', this.onRemoveAccount, this);
    this.accountsContainer.append(view.$el);
    this.accountViews.push(view);
  }

  saveData(e) {
    e.preventDefault();
    if(this.accountViews.length === 0) {
      this.accountsCountValidationText.show();
      return;
    }
    const allAccountsIsValid = this.accountViews.every(view => view.isValid());
    if(allAccountsIsValid) {
      this.model.save('accounts', this.accountViews.map(view => view.getData()));
      this.trigger('submit');
    }
  }

  addAccount() {
    this.collection.add({});
    const newAccount = this.collection.last();
    this.renderAccount(newAccount, this.collection.length - 1);
    this.emptyText.hide();
    this.accountsCountValidationText.hide();
  }

  onRemoveAccount(view) {
    this.accountViews = _.without(this.accountViews, view);
    if(this.collection.length === 0) {
      this.emptyText.show();
    }
  }

  gotoPreviousStep() {
    this.trigger('previous');
  }
}
