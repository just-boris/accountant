import Backbone from 'backbone';
import Syphon from 'backbone.syphon';
import template from './AccountView.hbs';
import IBAN from 'iban';
import validateBic from '../../util/validate-bic';

const IBAN_INVALID = 'IBAN must be in <a href="https://en.wikipedia.org/wiki/International_Bank_Account_Number">the following format</a>';
const BIC_INVALID = 'BIC must be in <a href="https://en.wikipedia.org/wiki/ISO_9362">the following format</a>';

export default class AccountView extends Backbone.View {
  events() {
    return {
      'click .account__remove': 'removeAccount',
      'change input': 'clearInvalid'
    };
  }

  render() {
    this.$el.html(template(this.model.toJSON()));
    return this;
  }

  removeAccount() {
    this.model.destroy();
    this.remove();
    this.trigger('remove', this);
  }

  getData() {
    return Syphon.serialize(this);
  }

  clearInvalid(e) {
    const field = this.$(e.currentTarget).parents('.form-group');
    field.removeClass('has-error');
    field.find('.help-block').html('');
  }

  markAsInvalid(input, message) {
    const field = input.parents('.form-group');
    field.addClass('has-error');
    field.find('.help-block').html(message);
  }

  isValid() {
    const data = this.getData();
    var valid = true;
    if(!IBAN.isValid(data.iban)) {
      this.markAsInvalid(this.$('[name=iban]'), IBAN_INVALID);
      valid = false;
    }
    if(!validateBic(data.bic)) {
      this.markAsInvalid(this.$('[name=bic]'), BIC_INVALID);
      valid = false;
    }
    return valid;
  }
}
