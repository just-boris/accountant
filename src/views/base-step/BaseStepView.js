import Backbone from 'backbone';

export default class BaseStepView extends Backbone.View {
  template() {
    throw new Error('template method must be implemented');
  }

  getViewData() {
    return this.model.toJSON();
  }

  render() {
    this.$el.html(this.template(this.getViewData()));
    return this;
  }
}
