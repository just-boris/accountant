import BaseStepView from '../base-step/BaseStepView';
import template from './SuccessView.hbs';

export default class SuccessView extends BaseStepView {
  initialize() {
    this.template = template;
  }
}
