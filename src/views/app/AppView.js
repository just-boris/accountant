import Backbone from 'backbone';
import template from './AppView.hbs';
import PersonalView from '../personal/PersonalView';
import AccountsListView from '../accounts-list/AccountsListView';
import SuccessView from '../success/SuccessView';
import router from '../../router';
import AppModel from '../../models/AppModel';

export const STEP_VIEWS = {
  'personal': PersonalView,
  'accounts': AccountsListView,
  'success': SuccessView
};
export const STEP_NAMES = Object.keys(STEP_VIEWS);

export default class AppView extends Backbone.View {
  initialize() {
    this.model = new AppModel();
    this.model.fetch();
    router.on('route', this.onRouteUpdate, this);
    this.currentStep = 0;
  }

  render() {
    this.$el.html(template({}));
    this.contentEl = this.$('.modal-content');
    this.renderStepView(this.currentStep);
    this.$('.modal').modal({
      keyboard: false,
      backdrop: 'static'
    });
    return this;
  }

  renderStepView(index) {
    const stepName = STEP_NAMES[index];
    const StepView = STEP_VIEWS[stepName];
    if(this.stepView) {
      this.stepView.remove();
    }
    this.stepView = new StepView({model: this.model}).render();
    this.stepView.$el.appendTo(this.contentEl);
    this.stepView.on({
      submit: this.nextStep.bind(this),
      previous: this.previousStep.bind(this)
    });
  }

  onRouteUpdate(name, [step]) {
    this.currentStep = STEP_NAMES.indexOf(step);
    if(this.currentStep === -1) {
      this.currentStep = 0;
    }
    this.renderStepView(this.currentStep);
  }

  nextStep() {
    this.currentStep++;
    if(this.currentStep < STEP_NAMES.length) {
      router.toUrl(STEP_NAMES[this.currentStep]);
    }
  }

  previousStep() {
    this.currentStep--;
    if(this.currentStep >= 0) {
      router.toUrl(STEP_NAMES[this.currentStep]);
    }
  }
}
