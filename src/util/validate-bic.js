const CHECK_REGEXP = /^[A-Z]{4}[A-Z]{2}[A-Z0-9]{2}([A-Z0-9]{3})?$/;

export default function(bic) {
  return CHECK_REGEXP.test(bic);
}
