import Backbone from 'backbone';

class AppRouter extends Backbone.Router {
  constructor() {
    super({
      routes: {
        '*default': 'step'
      }
    });
  }

  toUrl(url, options) {
    return this.navigate(url, Object.assign({trigger: true}, options));
  }
}

export default new AppRouter();
