Account App
==========

Web application for bank account management, built with Backbone.

### Setup

To start this application, do the following

1. Install [Node.js](https://nodejs.org/en/). Only Node.js 4+ is supported, because of
[jsdom](https://github.com/tmpvar/jsdom) package requirements
2. Open project folder
3. Run `npm install`

Then you can do the following:

1. **Build** Run `npm run build` to get bundled version of application. Resulting files will be in the `dist/` folder
2. **Develop** Run `npm start` to start development server. By default, it will be opened on http://localhost:3000. You can change
port using PORT environment variable
3. **Test** Run `npm test` to perform unit-tests with Jasmine
4. **Test coverage**. Run `npm run coverage` to gather test coverage

### Author

Boris Serdiuk <just-boris@hotmail.com>
