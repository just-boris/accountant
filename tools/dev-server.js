/*eslint-env node*/
var webpack = require('webpack');
var config = require('../webpack.config').configFactory(true);
var WebpackDevServer = require('webpack-dev-server');
var port = process.env.PORT || 3000;

config.entry.unshift(`webpack-dev-server/client?http://localhost:${port}`, 'webpack/hot/dev-server');
var compiler = webpack(config);
var server = new WebpackDevServer(compiler, {
    contentBase: './dist',
    stats: { colors: true },
    inline: true,
    hot: true
});
server.listen(port);
